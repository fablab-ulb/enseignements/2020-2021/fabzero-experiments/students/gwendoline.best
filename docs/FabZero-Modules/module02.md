# CONCEPTION ASSISTEE PAR ORDINATEUR

<style>body {text-align: justify}</style>

Deuxième semaine de cours : "_Et encore des lignes de commandes, mais pas que...un peu de dessin tout de même..._ "

_**Objectifs de ce module**_

>* _Comprendre et choisir le bon outil pour dessiner un pièce en 3D_
> *  _Apprendre à utiliser OpenSCAD_
>* _Apprendre à utiliser FreeCAD_

 _**Exercices de la semaine**_
> * _Choisir un Flexilinks sur le site de [BUY CMR](https://www.compliantmechanisms.byu.edu/maker-resources) et le dessiner sur un des programme de modélisation 3D_
>* _Designer un Flexilinks et réaliser son tutoriel_ afin de créer un kit Lego.


## Le bon outil de dessin 3D

Avant de se lancer dans la réalisation d'un dessin en 3D, il est toujours important de se poser la question suivante :

__"Pourquoi le fait-on ?"__

 Cette question permet de choisir le bon outil pour la bonne chose. Cela nous évite de permettre du temps inutilement. Cette question peut évidement s'appliquer à toutes autres choses...

 En conception 3D, il existe deux grandes familles de logiciels : la première étant les logiciels dits __graphiques__ et la seconde les logiciels dits __de codage__.

 Les logiciels nécessitants du codage ne demandent pas de se balader avec sa souris dans l'écran pour dessiner son modèle 3D, tandis que les logiciels graphiques demandent à dessiner trait par trait les lignes de son modèle 3D.

 Quelques exemples de logiciels pour comprendre...

 * __FreeCAD__ : Logiciel fait pour faire du "graphique"
 * __OpenSCAD__ : Logiciel fait pour faire "du codage"
 * __Rhinoceros 3D et Grashopper__ : Ce sont deux logiciels qui intègrent tant le graphique que le codage.

 Aujourd'hui, nous avons eu une initiation à OpenSCAD FreeCAD, ils respectent des logiques totalement différentes :

 * __OpenSCAD__ = description de la forme
 * __FreeCAD__ = contrainte de la forme

 Il faut tout de même noter qu'il est possible de combiner les deux logiques : on réalise les formes simples dans OpenSCAD en premier temps et en second temps réàliser les finitions dans FreeCAD.

## OpenSCAD

OpenSCAD est un logiciel où l'on décrit ses pièces __en codant__. On écrit les choses de __manière paramétrique__. La complexité avec OpenScad est savoir comment **décrire sa pièce au mieux pour pouvoir la coder.**

Il est très important de bien nommer tout son code pour pouvoir s'y retrouver et modifier certains paramètres si nous nous rendons compte que notre pièce doit être ajustée. Cela nous évite de devoir faire le travail deux fois. Il faut donc penser à bien __segmenter ses codes pour les rendre modulaires__.

Il existe **des feuilles qui répertorient des lignes de codes** pour réaliser des formes simples, des manipulations,...dans OpenSCAD. Tu peux emprunter celle de Nicolas afin de réaliser tes designs. Tu peux la retrouver [ici](https://www.openscad.org/cheatsheet/).


## FreeCAD

FreeCAD est un logiciel de CAO **paramétrique**. On va dessiner son design en lui donnant **des contraintes**. Ces paramètres font partie de l'objet, et restent modifiables n'importe quand après la création de l'objet.

Il permet de **multiples utilisations**, et permet de produire des modèles de toutes les tailles et de tous les objets. FreeCAD permet un plus **grande liberté dans se forme au contraire de OpenSCAD.** Cependant, il existe une **intégration de OpenSCAD dans FreeCAD**, c'est à dire qu'il est possible de réaliser une modélisation simple dans OpenSCAD et puis de venir le **paufiner et complexifier** dans FreeCAD.

Il existe plusieurs tutoriel qui t'expliquent les bases de FreeCAD sur le site de la Fabacademy. Tu peux les retrouver, [ici](http://academy.cba.mit.edu/classes/computer_design/index.html).

## Dessiner un Flexilinks - FreeCAD

## Designer un Flexilinks - Tutoriel
