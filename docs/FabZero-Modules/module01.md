# DOCUMENTATION

<style>body {text-align: justify}</style>

Première semaine de cours : "_Le grand saut !_ "  

___Objectifs de ce module___

>* _Installation GIT : un outil de documentation_
>* _Utilisation d'Atom : un outil de gestion des fichiers GitLab et éditeur de texte_
>* _Modifier  son site_
>* _FAQ_


## Installation de GIT

#### Etape 1

Pour **documenter et partager** son projet de manière **rapide et efficace**, télécharger Git sur son ordinateur. Tu peux le télécharger via ce lien : [_GitLab_](https://git-scm.com/). Il suffit juste d'**exécuter** le fichier téléchargé.


Pour **faire tourner GitLab**, il est nécéssaire d'installer le terminal BASH, ce dernier est natif dans **Windows 10**. Cette étape est assez facile, tout ce dont tu as besoin, t'es expliqué dans [ce tuto](https://blog.ineat-group.com/2020/02/utiliser-le-terminal-bash-natif-dans-windows-10/) sur Ineat-blog.

**Vérification** (Ouvrir le _terminal_ et taper les commandes suivantes)
```
git --version
```
Réponse attendue

```
git version X.Y.Z
```
GitBash est bien installé.

![image](../images/git1.JPG)

#### Etape 2

Maintenant que GitBash est installé, il faut le **configurer** en t'identifiant via le terminal en tapant ces quelques lignes de commandes.

```
git config --global user.name "your_username"
```
Ton nom d'utilisateur est celui que tu utilises pour t'identifier sur GitLab.
```
git config --user.email "your_email_address@example.com"
```
Tu dois indiquer l'adresse email que tu as utilisée pour créer ton compte sur GitLab.

* **Vérification**

```
git config --global --list
```
Réponse attendue.
```
user.name=User_name
user.email=email_adress@example.com
```
![image](../images/git2.jpg)

#### Etape 3

Après avoir configuré GitLab, maintenant, tu peux **associer** ton ordinateur à ce dernier. Cette étape est nécéssaire si tu veux **travailler de manière locale** avant de pouvoir envoyer ta documentation sur le serveur distant de Git.

Pour jumeler ton ordinateur à GitLab, tu dois ajouter des informations de sécurité. Tu peux ajouter une **clé SSH**. Cette clé permet de t'authentifier.

* **Comment créer une clé SSH**

Il en existe deux, la **ED25519** et la **RSA**. Ici, je t'explique comment réaliser une ED25519, elle est plus sure que la RSA.

Il faut **ouvrir le shell Bash** pour pouvoir taper les commandes suivantes.
```
ssh-keygen -t ed25519 -C "<comment>"
```
 La réponse attendue :
 ```
Generating public/private ed25529 key pair.
Enter file in which to save the key (/home/user/.ssh/id_ed25519):
 ```
 Après avoir reçu cette réponse, il faut appuyer sur ``enter`` de manière successive deux fois avant d'obtenir la clé comme montré ci-dessus et enfin terminer votre clé SSH.

![image](../images/git3.jpg)


* **Ajouter sa clé SSH au compte GitLab**

Facile, se rendre sur son compte GitLab. Cliquer sur la pastille circulaire en haut à droite > **Editer profil** > **Clé SSH** (se trouve dans l'onglet sur ta droite).

**Ouvrir la clé SSH**(.pub), copier et coller la dans l'encart prévu dans Git. Cliquer sur "**Ajouter**". C'est fait !

* **Vérification**

Il existe une double vérification...La **première** : tu vas recevoir un mail de GitLab t'indiquant qu'une clé SSH a été ajoutée à ton compte. La **seconde**, en tapant cette commande.

```
ssh - T git@gitlab.com
```


#### Etape 4
Comme expliqué précédemment, tu peux **travailler de manière locale**. Il faut alors créer un **référentiel distant**. Cela te permetra de travailler depuis ton ordinateur, et puis d'**envoyer tes modifications sur Git**.

Il faut alors se rendre sur son compte GitLab, cloner son dossier en choisissant sa clé SSH. Il faut copier le lien proposer.

Ensuite, il faut créer un dossier sur ton ordinateur où tu veux déposer ton **référentiel**. Tu dois y ouvrir **Gitbash dans le dossier et cloner ta clé SSH** préalablement copiée. Tu dois entrer les **commandes** suivantes et **ajouter ta clé SSH à la suite**.

```
git clone
```


## Utilisation d'Atom

Maintenant que notre dossier Git est copié sur notre ordianteur, nous pouvons travailler directement sur nos fichiers, en ajouter et en supprimer. Pour modifier le texte et fichiers, tu devras utiliser un éditeur de texte : **Atom**.
Cela te permettra de travailler sur Git à partir d'Atom. Atom permettra de modifier tes textes, mais aussi te permettra de gérer tout ton projet à distance.

#### Etape 1: Modifier ses fichiers

Ouvrir **son dossier Git dans Atom**.

```
Ouvrir Atom> File> Add project folder
```
Tu verras alors que dans le menu déroulant à ta gauche se trouve ton projet et tous les fichiers associés. Tu peux cliquer dessus et les modifiers directement en utilisant le **Markdown**.

* __Lien utile__ : [_Apprends les bases du Markdown_](https://www.markdowntutorial.com/)

Tu peux modifier tant les textes, qu'ajouter des photos, qu'ajouter des fichiers, ...

#### Etape 2 : Envoyer ses modifications de fichiers :  Pull & Push

GitLab et Atom sont liés.

* **Push** ( Envoyer des modifications apportées depuis Atom sur Gitlab )

Tu as fini de modifier tes documents dans Atom, pas de souci, tu peux **envoyer tes fichiers modifiés sur GitLab**.


1. Vérifier que je suis bien connecté à Git depuis Atom ![image](../images/A1.jpg) (il faut que le petit **icône du livre soit actif** ) et bien enregistrer mon travail ``Save``
2. Cliquer sur l'onglet ``Git`` en bas à droite dans Atom ![image](../images/gitlogo.jpg) .  Un onglet va se déployer.
3. Se rendre dans ``Unstaged Changes`` (il faut cliquer sur le petit logo en bas à droite nommé Git)
4. Faire ``Stage All``
5. ``Commit to master``
6. Enfin appuyer ``Push``


* **Pull** ( Envoyer des modifications apportées depuis Gitlab sur Atom )

Tu as modifié des documents sur Git que tu voudrais **récupérer sur Atom** pour pouvoir les travailler sur ton ordinateur. Il suffit de suivre ces étapes.


1. ``Ouvrir son projet`` dans Atom
2. Vérifier que je suis bien connecté à Git depuis Atom ![image](../images/A1.jpg) (il faut que le petit **icône du livre soit actif**)
3. Appuyer sur ``Fetch``
4. Enfin appuyer sur ``Pull``



## Thème du site web

Pour modifier l'apparence de son site Web, il faut trouver le mkdocs.yml dans votre dossier Git. Ce fichier sert à paramétrer son site web. Comme montrer ci dessous, il faut respecter les identifications à personnaliser. C'est à dire que "Arsène Lupin" devient dans mon cas, _Gwendoline Best_.

Si tu souhaites modifier l'apparence du site, tu peux modifier la couleur, la typographie, le menu déroulant. En te rendant sur [Mackadocks Boostwach](https://mkdocs.github.io/mkdocs-bootswatch/?fbclid=IwAR2cojBDtkeUeoi47RgVidXlxqEEEgz7H4wbAiy_CHaIIV-mO5pMfJLkgG4), tu peux choisir parmi une quinzaine de thèmes prêts à l'emploi. Dans mon cas, j'ai choisi le thème "``lux``"

Une fois toutes tes modifications réalisées, il suffit d'``Editer``.

![image](../images/strip.jpg)


## Mise en page

Utilisation du html pour modifier l'apparence de son site. Voici quellques lignes de commandes te permettant de créer des blocs html et ainsi customiser l'apparence de ton site.

#### Le corps de texte  

* __Centrer - aligner à gauche ou à droite__ son texte
```
  <div style="text-align: center"> your-text-here </div>
```
Pour aligner son texte à gauche ou à droite, il suffit de remplacer le "``center``" par "``left``" ou "``right``"

* __Justifier__ son texte   
```
<style>body {text-align: justify}</style>
```
* Mettre **en gras** son texte

```
<b>This text is bold </b>
```

#### Les images et photos

* __Centrer__, **aligner à gauche ou à droite** une image
```
<p align="center">
<img src="images/images.jpg">
</p>
```
Pour aligner son image à gauche ou à droite, il suffit de remplacer le "``center``" par "``left``" ou "``right``"

* __Redimensionner__ ses images

>__N.B.__ Sur la plateforme GitLab, il est important de ne pas déposer des fichiers "images" trop lourd. Il est souvent nécéssaire de les __compresser__ ou les __redimensionner__. Il est préférable de déposer des fichiers inférieur à **100 Ko**.


Il existe deux manière de précéder pour réduire le poids de nos images. Le première est d'utiliser un logiciel de retouches, tel que _Photoshop ou Gimp_, la seconde est d'utiliser _GraphicMagick_. GraphicMagik est un __logiciel libre__, en __ligne de commandes__. Il permet de créer, convertir, coller des images dans un grand nombre de formats. Il permet également de faire quelques rendus.

Pour __redimensionner une image__ avec GraphicMagick, il suffit d'ouvrir le GitBash dans un dossier contenant la photo à redimensionner et taper la commande suivante.

```
gm convert -resize 600x600 bigimage.png smallimage.jpg
```
Il faut remplacer le ``bigimage.png`` par le nom de votre fichier.

* __Créer un collage__ d'images

Ouvrir le GitBash et taper la commande suivante.

```
gm convert +append -geometry x400 image1.png image2.png image3.png image4.png strip.png
```
Il existe encore d'autres possibilités, te permettant d'ajouter des filtres, des formes sur tes images,... Tu trouveras une centaine de commandes te permettant de t'amuser en te rendant sur le site de [GraphicMagick](http://www.graphicsmagick.org/convert.html).
## FAQ

* **Je ne peux pas "PUSHER" mes modifications depuis Atom ?**

Le message d'erreur suivant apparait ...
> **"The tip of your current branch is behind its remote counterpart. try pulling before pushing to force push, hold cmd or ctrl while clicking."**

Cela veut dire qu'il est probable que vous n'avez mis à jour votre dossier référent sur votre ordinateur et que vous avez fait des mises à jour directement sur la plateforme Git. Pas de panique, cela peut se résoudre assez rapidement. Il faut faire un "**Pull**" avant de pouvoir à nouveau "**Pusher**" vos modifications. Il faut se rendre en bas de la fenêtre de Atom faire un "clic-droit" sur l'icône "Push" et puis sélectionner "Pull".
