# ELECTRONIQUE I

<style>body {text-align: justify}</style>

Cinquième semaine de cours : "_Un peu d'éléctro !_ "  


## ARDUINO - UN ENVIRONNEMENT

#### Qu'est ce que c'est ?

**Arduino** est la marque d'une **plateforme de prototypage open-source** qui permet aux utilisateurs de créer des objets électroniques interactifs à partir de cartes électroniques sur lesquelles se trouve un **micro-contrôleur.**

#### La carte Arduino

La carte Arduino permet à partir d'évènements détectés par des capteurs, de programmer et commander des actionneurs. C'est un module **micro-controleur**.

**Sa composition ...**

![](https://www.editions-eni.fr/Open/download/c5c96936-e51f-42b6-b490-1ea441d1a8ac/images/02Ar08.png)


L'un des élément les plus important de la carte est la pièce **ATMEGA 328P.** Ce petit élément est le **microprocesseur** ; c'est lui qui va exécuter les informations programméés, en gros, c'est le cerveau de la carte. Il faut savoir que c'est le **seul** élement de la carte qui peut être **retirable** et **changeable**.

Avant de pouvoir utiliser, le **microprocesseur**, il est nécéssaire d'utiliser un **bootloader** qui est un microprogramme qui nous permettra d'établir une connexion avec notre ordinateur, cette étape nous évite d'avoir un programmateur externe en Hardware. C'est l'**ISCP** qui permettra d'envoyer ces informations au microprocesseur.

_NB:_ L'**Atmega** possède déjà un microprogramme de programmation.

#### Intérets et applications de l'environnment Arduino ?

* **Intérets** : En Open-source, pré-programmés, nécéssite peu de connaissances en éléctronique, bien documenté,...
* **Applications** : Capteur de température, capteur de CO2,...

## PREALABLE

Avant de se lancer dans la programmation et l'assemble des différents éléments nécéssaires à créer ton objet intérractif, il faut tout d'abord :

1. Installer le **programme** d'Arduino. Le logiciel se trouve sur le [site d'Arduino](https://www.arduino.cc/en/software).
2. Connaitre quelques bases du **vocabulaire en électronique et programmation**. Tu trouveras tous le nécéssaire pour commencer dans ce manuel. [ICI](http://perso-laris.univ-angers.fr/~cottenceau/ArduinoCottenceau2016.pdf)

## PROTOTYPE I - FAIRE CLIGNOTER UNE LED

#### Ce dont tu as besoin

![image](../images/arduino.jpg)

#### Le montage
#### Le code à programmer

Tu peux trouver cet exemple de code directement dans le logiciel d'Arduino.
```
// the setup function runs once when you press reset or power the board
void setup() {
  // initialize digital pin LED_BUILTIN as an output.
  pinMode(10, OUTPUT);
}

// the loop function runs over and over again forever
void loop() {
  digitalWrite(10, HIGH);   // turn the LED on (HIGH is the voltage level)
  delay(1000);                       // wait for a second
  digitalWrite(10, LOW);    // turn the LED off by making the voltage LOW
  delay(1000);                       // wait for a second
}
```

#### Envoyer l'information à la carte

* **Etape 1 :** Il faut connecter la carte Arduino à ton ordinateur avec le cable USB.

* **Etape 2 :** Vérifier ta programmation en appuyant sur l'icône "V" sur le programme d'Arduino. Si tout est OK, aucun message d'erreur n'apparaitra.

* **Etape 3 :** Téléverser les infos ; il faut appuyer sur l'icône "Flèche vers la droite", c'est cet outil qui te permet d'envoyer les infos au micro-processeur.

>**Il arrive parfois qu'il ne se passe rien...**
>
>**Trois possibilités ...**
>* **La première** : Ton cable d'alimentation n'est pas bien connecté et l'ordinateur ne détecte pas ta carte.
>* **La seconde** : Tu n'as pas choisi le bon port USB. Tu peux le régler facilement en te rendant dans Outil > Port > et choisir le bon port.
>* **La troisième** : Tu as cramé ta carte...

* **Etape 4 :** Si tout est OK, la LED se mettra  clignoter.



## PROTOTYPE II - COMMANDER DES FEUX DE SIGNALISATION
