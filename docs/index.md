## QUI SUIS-JE ?

<p align="center">
<img src="images/portrait.jpg">
</p>


<style>body {text-align: justify}</style>


Gwendoline est le nom qui m'a été donné à la naissance, mais qui depuis a toujours été raccourci en "Gwen". Radical.
Cela me convient peut-être mieux, peut-être parce que ce petit nom, très _court et simple_, est le reflet d’un tempérament _franc et sans détour_.

<b>_Tout commença par une punition…_</b>

Ne dit-on pas que :

«_L’art nait de la contrainte et meurt dans la liberté_», _Michel Ange_

« Etant petite, ma mère avait pour habitude de punir ou calmer mon excès d’énergie par des activités créatives en tout genre : _pâte à modeler, papier macher, peinture, enfilage de perles, moulages, tricots, points de croix, couture, dessin …_ Ces activités permettaient de me calmer, de me recentrer et de faire le point. Ces activités étaient libératrices. »
Depuis, l’acte créatif est un vrai exutoire. C’est peut-être ce qui m’a motivée à suivre des études en Architecture.

Formée aux arts graphiques et diplômée d’un master en Architecture, aujourd’hui, j’ai rejoint l’équipe du [FabLab de l’ULB](http://fablab-ulb.be/)  en tant Fabmanageuse.

_**Le monde du Fablab**..._

En tant que Fabmanageuse, nos nombreuses activités et préoccupations ne me permettent pas de pouvoir bénéficier du terrain de jeux qu’est le FabLab.

_Intuitive et sensible_, j’aime découvrir des nouveautés, approcher des idées sous un regard neuf et original, … J’aime m’imprégner d’une atmosphère, d’une ambiance pour trouver ma motivation. C’est certainement en écoutant tous mes sens en observant, en écoutant, en touchant - que j’apprends et que j’évolue le mieux.  

C’est pourquoi, je me suis décidée à suivre ce cours en immersion totale. De plus, la **pédagogie par le faire** dite active,  m’intéresse fortement. Cette thématique était une des pierres fondatrices de mon mémoire de fin d’étude. Il était question de reconsidérer et critiquer la pédagogie et l'enseignement en Architecture.
